class Adress{

  int _id;
  String _descripcion;
  String _colonia;
  String _localidad;
String _estado;
String _cp;

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get descripcion => _descripcion;

  String get cp => _cp;

  set cp(String value) {
    _cp = value;
  }

  String get estado => _estado;

  set estado(String value) {
    _estado = value;
  }

  String get localidad => _localidad;

  set localidad(String value) {
    _localidad = value;
  }

  String get colonia => _colonia;

  set colonia(String value) {
    _colonia = value;
  }

  set descripcion(String value) {
    _descripcion = value;
  }



  Adress(this._id, this._descripcion, this._colonia, this._localidad,
      this._estado, this._cp);

  @override
  String toString() {
    return 'Auxiliar{_id: $_id, _descripcion: $_descripcion, _colonia: $_colonia, _localidad: $_localidad, _estado: $_estado, _cp: $_cp}';
  }
}