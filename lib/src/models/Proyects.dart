class Proyects {

  int id;
  String descripcion;
  String _fecha;



  int get get_id{
    return id;
  }

  void set set_id(int id) {
    this.id = id;
  }

  String get get_descripcion{
    return descripcion;
  }

  void set set_descripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  String get fecha => _fecha;

  set fecha(String value) {
    _fecha = value;
  }

  Proyects(this.id, this.descripcion,this._fecha);

  @override
  String toString() {
    return 'Proyects{id: $id, descripcion: $descripcion}';
  }
}