
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/models/Files.dart';
import 'package:isae/src/services/ApiDefinitions.dart';
import 'package:isae/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:isae/src/ui/Frame/InventarioAdd_scren.dart';
import 'package:isae/src/ui/Frame/MensajeInicioSesion_scren.dart';
import 'package:isae/src/ui/Frame/PthotoImagen_scren.dart';
import 'package:isae/src/ui/Frame/Inventario_scren.dart';
import 'package:isae/src/ui/loginNewUser_screen.dart';
import 'package:isae/src/ui/loginUpdatePassReset_screen.dart';
import 'package:isae/src/ui/loginUpdatePass_screen.dart';
import 'package:isae/src/ui/login_screen.dart';
import 'package:camera/camera.dart';
class ImcRBloc {


  static  validateUserLogin(BuildContext context,int opcion, int olvido) async {

    print('Entrando a app cargando Ws -----> Login');
    if(opcion == 1){
      loginUser(context, ApiDefinition.wsLoginUser, GlobalVariables.textNEmp.text, GlobalVariables.textPass.text);

    }else{
      loginValidateUser(context, ApiDefinition.wsLoginValidateUser, GlobalVariables.textNEmp.text, GlobalVariables.textPass.text,olvido);
    }


  }
  static  updateUserLogin(BuildContext context) async {


      updateUser(context, ApiDefinition.wsLoginUserUpdate + GlobalVariables.user.id.toString(), GlobalVariables.textPassNew.text);


  }
  static  updateUserResetLogin(BuildContext context) async {


      loginValidateUserReset(context, ApiDefinition.wsLoginValidateUserCorreo, GlobalVariables.userReset.text, GlobalVariables.textPass.text);

  }
  static  resetPass(BuildContext context) async {

    print('Entrando a app cargando Ws -----> Login');
    resetPassword(context, ApiDefinition.wsEnvioMailResetPass);


  }
  static getProyects(BuildContext context,int opcion, String tipo, String descripcionProyect) async {

    print('Entrando a app cargando Ws -----> Home');
    GlobalVariables.DescripProyect=descripcionProyect;
    getProyect(context, ApiDefinition.wsProyects+'/${tipo}', opcion);
    //getInventarioAgrupado(context, ApiDefinition.wsInventToAgrupado+ GlobalVariables.idProyect.toString()+ '/' + GlobalVariables.user.id.toString(), '2019-06-30', '206246');
    for (var index = 1; index < 30; index++){
      ImcRBloc.loadcatlogos(context,index);

    }


  }
  static loadInventarioAgrupado(BuildContext context, int idProyect, int idInven ) async {

    print('Entrando a app cargando Ws -----> Home');
    getInventarioAgrupado(context, ApiDefinition.wsInventToAgrupado+ GlobalVariables.idProyect.toString()+ '/' + GlobalVariables.user.id.toString(), '2019-06-30', '206246');


  }
  static loadInventarioBuscar(BuildContext context, String idProyect, String folio ) async {

    print('Entrando a app cargando Ws -----> Home');
    getInventarioBuscar(context, ApiDefinition.wsInventToBuscar+ idProyect+ '/' + GlobalVariables.user.id.toString(), folio);



  }
  static geMaxInventarioByProyect(BuildContext context) async {

    print('Entrando a app cargando Ws -----> Home');
    getMaxInvent(context, ApiDefinition.wsMaxInventByProyect+ GlobalVariables.idProyect.toString());

  }
  static loadIProyectBuscar(BuildContext context, String folio ) async {

    print('Entrando a app cargando Ws -----> Home');
    getProyectoBuscar(context, ApiDefinition.wsProyectToBuscar, folio);



  }
  static loadInventarioDetalleAgrupado(BuildContext context,int opcion) async {


    if( opcion == 1){
      print('Entrando a app cargando Ws -----> DetalleAgrupado id usuario editar ' +  GlobalVariables.user.id.toString());
      getInventarioDetalleAgrupado(context, ApiDefinition.wsDetalleAgrupado + GlobalVariables.idProyect.toString()+ '/' + GlobalVariables.idIventario.toString() + '/' +GlobalVariables.idAgrupador.toString()+ '/' + GlobalVariables.user.id.toString());

    }else{
      print('id de usuario pivot add '+ GlobalVariables.user.id.toString());
      getInventarioDetalleAgrupado(context, ApiDefinition.wsDetalleAgrupadoAdd + GlobalVariables.idProyect.toString()+ '/' +GlobalVariables.idAgrupador.toString()+ '/' + GlobalVariables.user.id.toString());

    }




  }
  static updateDetalleInvet(BuildContext context, int idProyc, int idInvent, String campo, String valor) async {

    //print('Entrando a app cargando Ws -----> update  ' + campo + ' ' + valor );
    updateInvent(context, ApiDefinition.wsUpdateInventario + idProyc.toString()+ '/' + idInvent.toString(),campo,valor );
    //getInvent(context, ApiDefinition.wsInventToProyect + '6');



  }


  static loadcatlogos(BuildContext context, int id) async {

    print('Entrando a app cargando Ws -----> catalogos ' + id.toString());
    getCatalogosAdd(context, ApiDefinition.wsCatalogos+ id.toString(), id);


  }

  static insertInvet(BuildContext context) async {
    try{
    print('Entrando a app cargando Ws -----> update');
    insertInvent(context, ApiDefinition.wsInsertInvet);
    }on Exception catch (exception) {
      showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $exception: conexión - service'));
      print("throwing new error");
      throw Exception("Error on server");
    }

  }

  static  getAdressCP(BuildContext context, String CP) async {


    getWsAdressCP(context,ApiDefinition.wsAdressCP, CP);

  }

  static  getCatalogosAdress(BuildContext context, String wsApi, String parametro, int numCat) async {


    getWsAdressCatCP(context,wsApi, parametro, numCat);

  }

  static  getMarcaModelo(BuildContext context, String wsApi, String parametro, int numCat, String campo) async {
    getwsMarcaModelo(context,wsApi, parametro, numCat, campo);
  }

  static getFilesIvent(BuildContext context) async {

    print('Entrando a app cargando Ws -----> Files...' );
    getFilesInv(context, ApiDefinition.wsGetFiles);


  }
  static getFilesIventByNoSerie(BuildContext context, String fileName) async {

    print('Entrando a app cargando Ws -----> Files...' );
    getFilesInvByNoSerie(context, ApiDefinition.wsGetFilesByNoSerie, fileName);


  }
  static getFilesIventValidate(BuildContext context, String fileName) async {

    print('Entrando a app cargando Ws -----> Files...' );
    getFilesInvValidate(context, ApiDefinition.wsGetFilesValidate,fileName);


  }
  static InsertFiles(BuildContext context, Files files) async {

    print('Entrando a app cargando Ws -----> Inserta Files... ' + files.fileUrl);
    insertFilesInv(context, ApiDefinition.wsInsertFiles, files);


  }
  static InsertFilesOtros(BuildContext context, Files files) async {

    print('Entrando a app cargando Ws -----> Inserta Files... ' + files.fileUrl);
    insertFilesInvOtros(context, ApiDefinition.wsInsertFilesOtros, files);


  }

  static eliminaFiles(BuildContext context, Files files) async {

    print('Entrando a app cargando Ws -----> elimina Files... ' + files.id.toString());
    deleteFilesInv(context, ApiDefinition.wsdeleteFiles, files);


  }

  static getFilePDF(BuildContext context, File firma1, File firma2, File firma3, File firma4, String nameFile) async {

    print('Entrando a app cargando Ws ----->  genrar archivo PDF... ' );
    getFileFormat(context, ApiDefinition.wsInventFormat,firma1, firma2, firma3, firma4, nameFile);


  }

  static loadFoto(BuildContext context, int idM) async {
        Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => MensajeInicioSesionScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

//    GlobalWidgets.loadingIndicator(context, false);
//    final cameras = await availableCameras();
//
//    // Obtén una cámara específica de la lista de cámaras disponibles
//    final firstCamera = cameras.first;
//
//    Navigator.pushReplacement(
//        context,
//        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => TakePictureScreen(
//          // Pasa la cámara correcta al widget de TakePictureScreen
//          camera: firstCamera,
//        ),
//          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//          transitionDuration: Duration(seconds: 1),
//        )
//    );
//    Navigator.pushReplacement(
//        context,
//        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioScreen(),
//          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//          transitionDuration: Duration(seconds: 1),
//        )
//    );

//    Navigator.pushReplacement(
//        context,
//        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => PhotoPreviewScreen(),
//          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//          transitionDuration: Duration(seconds: 1),
//        )
//    );

  }
  static InicioSesion(BuildContext context) async{

    GlobalWidgets.loadingIndicator(context, false);
    final cameras = await availableCameras();

    // Obtén una cámara específica de la lista de cámaras disponibles
    final firstCamera = cameras.first;

    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => TakePictureScreen(
          // Pasa la cámara correcta al widget de TakePictureScreen
          camera: firstCamera,
        ),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );
  }
  static closeSesion(BuildContext context) async{
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => LoginScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }
  static UpdatePassLoginn(BuildContext context) async{
    GlobalWidgets.loadingIndicator(context, false);
    ImcRBloc.laoding(context, false);
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => LoginUpdatePassScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }
  static UpdatePassResetLoginn(BuildContext context) async{
    GlobalWidgets.loadingIndicator(context, false);
    ImcRBloc.laoding(context, false);
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => LoginUpdatePassResetScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }
  static newUserLoginn(BuildContext context) async{
    GlobalWidgets.loadingIndicator(context, false);
    ImcRBloc.laoding(context, false);
    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => LoginNewUserScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }

  static loadInventarioAdd(BuildContext context) async {

    GlobalWidgets.loadingIndicator(context, false);

    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioAddScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }
  static loadProyects(BuildContext context) async {

    GlobalWidgets.loadingIndicator(context, false);

    Navigator.pushReplacement(
        context,
        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioScreen(),
          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
          transitionDuration: Duration(seconds: 1),
        )
    );

  }

  static laoding(BuildContext context, bool bandera) async {

    GlobalWidgets.loadingIndicator(context, bandera);



  }

}