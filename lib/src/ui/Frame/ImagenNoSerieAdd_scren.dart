import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/models/Files.dart';
import 'package:isae/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:isae/src/ui/Frame/InventarioDetalleAddAgrupacion_scren.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'InventarioDetalleAgrupacion_scren.dart';

class TakePictureNoSerieAddScreen extends StatefulWidget {
  final CameraDescription camera;


  const TakePictureNoSerieAddScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureNoSerieAddScreenState createState() => TakePictureNoSerieAddScreenState();
}

class TakePictureNoSerieAddScreenState extends State<TakePictureNoSerieAddScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;


  @override
  void initState() {
    super.initState();
    // Para visualizar la salida actual de la cámara, es necesario
    // crear un CameraController.
    _controller = CameraController(
      // Obtén una cámara específica de la lista de cámaras disponibles
      widget.camera,
      // Define la resolución a utilizar
      ResolutionPreset.medium,

    );
    getUserLocation();
    // A continuación, debes inicializar el controlador. Esto devuelve un Future!
    _initializeControllerFuture = _controller.initialize();
    disablePathProviderPlatformOverride = true;
  }

  @override
  void dispose() {
    // Asegúrate de deshacerte del controlador cuando se deshaga del Widget.
    _controller.dispose();
    super.dispose();
  }
  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();
    setState(() {
      GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);
    });

    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tomar Foto '),
      elevation: 0.0,
      leading: GlobalWidgets.addLeadingIcon(),
      centerTitle: true,
      backgroundColor: Color.fromRGBO(12, 160, 219, 1),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.close),
          onPressed: () {

            Navigator.pushReplacement(
                context,
                PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioDetalleAddAgrupacionScreen(),
                  transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                  transitionDuration: Duration(seconds: 2),
                )
            );


          },
        ),
      ],
    ),
      // Debes esperar hasta que el controlador se inicialice antes de mostrar la vista previa
      // de la cámara. Utiliza un FutureBuilder para mostrar un spinner de carga
      // hasta que el controlador haya terminado de inicializar.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Si el Future está completo, muestra la vista previa
            return CameraPreview(_controller);
          } else {
            // De lo contrario, muestra un indicador de carga
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        // Agrega un callback onPressed
        onPressed: () async {
          // Toma la foto en un bloque de try / catch. Si algo sale mal,
          // atrapa el error.
          try {

            // Ensure the camera is initialized
            await _initializeControllerFuture;
            String fecha= new DateTime.now().toString();


           print('fecha:: ' +DateTime.now().toString());
           // Construye la ruta donde la imagen debe ser guardada usando
            // el paquete path.
            final path = join(

              //
              (await getTemporaryDirectory()).path,
              '${fecha}.png',
            );
            print('Rutaaaaa de la Foto:::: '+ path);
            // Attempt to take a picture and log where it's been saved
            await _controller.takePicture(path);

            // En este ejemplo, guarda la imagen en el directorio temporal. Encuentra
            // el directorio temporal usando el plugin `path_provider`.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // Si se produce un error, regístralo en la consola.
            print(e);
          }
        },
      ),

    );
  }




}

// Un Widget que muestra la imagen tomada por el usuario
class DisplayPictureScreen extends StatelessWidget {


  StorageReference storageReference;
  final String imagePath;
  String _uploadedFileURL;
  DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);
  @override
  void initState(){
    getUserLocation();
  }

  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();

    GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);


    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Numero de serie', maxLines: 1, textAlign: TextAlign.center, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white)),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioDetalleAddAgrupacionScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      body:
      Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[

              Container(
                height: MediaQuery.of(context).size.height * 0.03,

              ),
              Container(

                  child:   Image.file(File(imagePath),width: 500, height: 500),

              ),


              Container(
                height: MediaQuery.of(context).size.height * 0.03,

              ),

              Material(
                elevation: 5.0,
                //borderRadius: BorderRadius.circular(30.0),
                color:  Color.fromRGBO(74, 191, 37, 1),
                child: MaterialButton(

                  //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () async {


                    print('Guardar Foto numero de serie ' + GlobalVariables.campoNoSerieAdd);
                    String fileName = '${GlobalVariables.nameNoSerieAdd.replaceAll(' ', '-').replaceAll('.', '')}';

                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEEQUIPO'?    GlobalVariables.fileNameFotoNumSerieAddEQUIPO = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMONITOR'?   GlobalVariables.fileNameFotoNumSerieAddMONITOR = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIETECLADO'?   GlobalVariables.fileNameFotoNumSerieAddTECLADO = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMOUSE'?     GlobalVariables.fileNameFotoNumSerieAddMOUSE = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEUPS'?       GlobalVariables.fileNameFotoNumSerieAddUPS = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMALETIN'?   GlobalVariables.fileNameFotoNumSerieAddMALETIN = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECANDADO'?   GlobalVariables.fileNameFotoNumSerieAddCANDADO = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEBOCINAS'?   GlobalVariables.fileNameFotoNumSerieAddBOCINAS = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECMARA'?     GlobalVariables.fileNameFotoNumSerieAddCAMARA = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMONITOR2'?  GlobalVariables.fileNameFotoNumSerieAddMONITOR2 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEACCESORIO'? GlobalVariables.fileNameFotoNumSerieAddACCESORIO = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP1'?     GlobalVariables.fileNameFotoNumSerieAddCOMP1 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP2'?     GlobalVariables.fileNameFotoNumSerieAddCOMP2 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP3'?     GlobalVariables.fileNameFotoNumSerieAddCOMP3 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP4'?     GlobalVariables.fileNameFotoNumSerieAddCOMP4 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP5'?     GlobalVariables.fileNameFotoNumSerieAddCOMP5 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP6'?     GlobalVariables.fileNameFotoNumSerieAddCOMP6 = fileName :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP7'?     GlobalVariables.fileNameFotoNumSerieAddCOMP7 = fileName :
                    '';



                        File file = File(imagePath);
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEEQUIPO'?    GlobalVariables.fileNoSerieFotoAddEQUIPO = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMONITOR'?   GlobalVariables.fileNoSerieFotoAddMONITOR = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIETECLADO'?   GlobalVariables.fileNoSerieFotoAddTECLADO = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMOUSE'?     GlobalVariables.fileNoSerieFotoAddMOUSE = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEUPS'?       GlobalVariables.fileNoSerieFotoAddUPS = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMALETIN'?   GlobalVariables.fileNoSerieFotoAddMALETIN = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECANDADO'?   GlobalVariables.fileNoSerieFotoAddCANDADO = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEBOCINAS'?   GlobalVariables.fileNoSerieFotoAddBOCINAS = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECMARA'?     GlobalVariables.fileNoSerieFotoAddCAMARA = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEMONITOR2'?  GlobalVariables.fileNoSerieFotoAddMONITOR2 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIEACCESORIO'? GlobalVariables.fileNoSerieFotoAddACCESORIO = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP1'?     GlobalVariables.fileNoSerieFotoAddCOMP1 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP2'?     GlobalVariables.fileNoSerieFotoAddCOMP2 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP3'?     GlobalVariables.fileNoSerieFotoAddCOMP3 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP4'?     GlobalVariables.fileNoSerieFotoAddCOMP4 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP5'?     GlobalVariables.fileNoSerieFotoAddCOMP5 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP6'?     GlobalVariables.fileNoSerieFotoAddCOMP6 = file :
                    GlobalVariables.campoNoSerieAdd == 'NUMSERIECOMP7'?     GlobalVariables.fileNoSerieFotoAddCOMP7 = file :
                    '';
                    print('file save varibales ' + GlobalVariables.fileNoSerieFotoAddEQUIPO.path);
                    Navigator.pushReplacement(
                    context,
                    PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioDetalleAddAgrupacionScreen(),
                    transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
                    transitionDuration: Duration(seconds: 3),
                    )
                    );

                    showDialog(context: context, builder: (context) => ShowMessageSave('No Serie guardado correctamente!'));
                                  },
                  textColor: Colors.white,
                  child: Text("Guardar",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
  void getFiles(BuildContext context) async{
    ImcRBloc.getFilesIvent(context);
  }
  void insertNoSerie(BuildContext context, String fileName ) async{
                        print("Guardar::::...... ");



                    print(fileName);

                    // String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();
                    print('cargo foto ' + fileName);

                    LoginBloc.loadingI(context,true);


                    Directory documentDirectory = await getApplicationDocumentsDirectory();
                    String documentPath = documentDirectory.path;
                    String fileCopy = "$documentPath/$fileName";
                    File temp = File(imagePath);
                    File newFile = await temp.copy(fileCopy);
                    print('path antes:: ' + imagePath);
                     File fileFinal = new File.fromUri(Uri.parse(newFile.path));
                    print('fileFinal:: ' + fileFinal.path);



                    StorageReference storageReference = FirebaseStorage.instance
                        .ref()
                        .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idIventario}-Inventario/Evidencias/$fileName');
                    StorageUploadTask uploadTask = storageReference.putFile(temp);
                    await uploadTask.onComplete;
                    print('File Uploaded');

                    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
                    _uploadedFileURL = dowurl.toString();

                    print('urllllllllll  ' +_uploadedFileURL);

                    Files files = new Files(
                        0,
                        fileName,
                        _uploadedFileURL,
                        GlobalVariables.center.toString(),
                        GlobalVariables.user.id,
                        null,
                        GlobalVariables.idIventario,
                        GlobalVariables.idProyect,
                        'jpg'
                    );
                    print('Files ' + files.toString());

                    ImcRBloc.InsertFiles(context, files);
                    Future.delayed(Duration(seconds: 2), () => {
                      ImcRBloc.getFilesIvent(context)
                    });
                    Future.delayed(Duration(seconds: 4), () => {
                      LoginBloc.loadingI(context, false),
                      Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioDetalleAgrupacionScreen(),
                            transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
                            transitionDuration: Duration(seconds: 3),
                          )
                      )
                    });
                    showDialog(context: context, builder: (context) => ShowMessageSave('No Serie guardado correctamente!'));
  }
  showAlertNoSerieExist(BuildContext context, String fileName) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {
        insertNoSerie(context, fileName);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirmar"),
      content: Text("Existe una evidencia, ¿Desea reemplazar?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


}
Widget getGoogleMaps(BuildContext context, LatLng _center, Map<String, Marker> _markers) {
  if(_center != null)
    return Container(
      height: MediaQuery.of(context).size.height * 2.0,
      child:GoogleMap(
        myLocationButtonEnabled: true,
        buildingsEnabled: true,
        myLocationEnabled: true,
        trafficEnabled: true,
        mapType: MapType.hybrid,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 17,
        ),
        markers: _markers.values.toSet(),
      ),
    );
}