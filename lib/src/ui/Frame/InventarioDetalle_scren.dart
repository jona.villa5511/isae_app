import 'dart:async';
import 'dart:typed_data';


import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:isae/src/models/InventarioAgrupado.dart';
import 'package:isae/src/models/Proyects.dart';
import 'package:isae/src/services/ApiDefinitions.dart';
import 'package:isae/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:isae/src/ui/Frame/Firma1Screen.dart';
import 'package:isae/src/ui/Frame/InventarioDetalleAgrupacion_scren.dart';
import 'package:isae/src/ui/Frame/InventarioLoadFiles_scren.dart';
import 'package:isae/src/ui/Frame/Inventario_scren.dart';
import 'package:flutter/services.dart';
class InventarioDetalleScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => InventarioDetalleScreenState();

}

class InventarioDetalleScreenState extends State<InventarioDetalleScreen> {
  //String remotePDFpath = "";
  StorageReference storageReference;

  @override
  void initState(){
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown]);
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    //print('Fecha del dia  $currentTime');
    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,
      appBar:
      //PreferredSize(
        //  preferredSize: Size.fromHeight(85.0),
          //   child:
      GlobalWidgets.topBar('INVENTARIO AGRUPADO','Marco Antonio', context, '$currentTime'.substring(0,10)),

      //    ),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),


      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.topCenter,
                decoration: BoxDecoration(
                    color:  Color.fromRGBO(9, 99, 141, 1),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(0.0),
                        bottomRight: Radius.circular(0.0))),

                child:  Row ( children: <Widget>[
                  new Icon(Icons.work,color: Colors.white),
                  Text('Proyecto: ' + GlobalVariables.nameProyectFile, maxLines: 1, textAlign: TextAlign.center, style:
                  TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.02,
                  ),
                  new Icon(Icons.library_books,color: Colors.white),
                  Text('Inventario: ' + GlobalVariables.nameInvenFolioFile, maxLines: 1, textAlign: TextAlign.center, style:
                  TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),

                ]
                ),
              ),
              GlobalWidgets.menuHome(context,'$currentTime'.substring(0,10),botonRegresar(context)),
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[



                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        Container(
                            color: Color.fromRGBO(178, 222, 235, 1),
                          padding: EdgeInsets.fromLTRB(30, 1, 1, 10),
                          child: Column(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  LoginBloc.loadingI(context,true);
                                  print("Cargar archivos::::...... " + GlobalVariables.center.toString());
                                  getFiles(context);
                                  GlobalVariables.listFiles.clear();

                                  Future.delayed(Duration(seconds: 3), () => {
                                    LoginBloc.loadingI(context, false),

                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                        return InventarioLoadFilesScreen();
                                      }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                        return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                        ).animate(animation), child: child,);
                                      },
                                          transitionDuration: Duration(seconds: 1)
                                      ), (Route route) => false,)

                                  });
                                }, // handle your onTap here
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.search , color: Colors.blue),
                                    Text('CONSULTAR EVIDENCIA',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                            decoration:
                                            TextDecoration.underline,
                                            color: Color.fromRGBO(9, 99, 141, 1))),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.03,
                        ),
                        FlatButton(

                          onPressed: () => {

                            GlobalVariables.firma1 = ByteData(0),
                            GlobalVariables.firma2 = ByteData(0),
                            GlobalVariables.firma3 = ByteData(0),
                            GlobalVariables.firma4 = ByteData(0),
//
//                        Navigator.pushReplacement(
//                            context,
//                            PageRouteBuilder(pageBuilder: (context, anim1, anim2) => Firma1Screen(),
//                              transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//                              transitionDuration: Duration(seconds: 1),
//                            )
//                        )
                            Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                return Firma1Screen();
                              }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                ).animate(animation), child: child,);
                              },
                                  transitionDuration: Duration(seconds: 1)
                              ), (Route route) => false,)
                          },
                          color:  Color.fromRGBO(74, 191, 37, 1),
                          padding: EdgeInsets.all(5.0),
                          textColor: Colors.white,
                          child: Column( // Replace with a Row for horizontal icon + text

                            children: <Widget>[
                              Icon(Icons.mode_edit),
                          Text("Firmar Documento",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                            ],
                          ),
                        ),


                        Container(
                          height: MediaQuery.of(context).size.height * 0.03,

                        ),
                        Container(

                          color:  Color.fromRGBO(207, 227, 233, 1),
                          child: Column(
                            children: <Widget>[

                              Container(
                                color: Color.fromRGBO(207, 227, 233, 1),
                                child: getAgrupado(context, GlobalVariables.listAgrupado),
                              ),
                            ],
                          ),
                        ),

                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,

                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );


  }
  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();
    setState(() {
      GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);
    });

    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
}

void getFiles(BuildContext context) async{
  ImcRBloc.getFilesIvent(context);
}
Widget botonRegresar(BuildContext context) {
  return Container(
child:   FlatButton(
  child:
  Column( // Replace with a Row for horizontal icon + text
    children: <Widget>[
      Image(image: new AssetImage('assets/images/previous.png'),
          width: 25, height: 25, color: Colors.white),
      Text("Regresar",
        textAlign: TextAlign.center,
        style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Colors.white),
      )
    ],
  ),
  onPressed: (){

    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(

          pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioScreen();
          },

          transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          },

          transitionDuration: Duration(seconds: 1)
      ),

          (Route route) => false,
    );
  },
),
  );
}
Widget getAgrupado(BuildContext context , List<InventarioAgrupado> listaAgrupado) {


  return Container(
    color: Color.fromRGBO(178, 222, 235, 1),

    padding: EdgeInsets.all(20),

    child: Column(children: <Widget>[

    for(var index = 0; index < listaAgrupado.length; index++)(


        InkWell(
          onTap: () {
        GlobalVariables.idAgrupador = GlobalVariables.listAgrupado[index].id;
        GlobalVariables.nameAgrupador = GlobalVariables.listAgrupado[index].descripcion;
        print('Detalle Campos ..........    ' + GlobalVariables.listAgrupado[index].id.toString() + ' Agrupador:: ' +  GlobalVariables.nameAgrupador);

        LoginBloc.loadingI(context,true);
        ImcRBloc.loadInventarioDetalleAgrupado(context,1);

        Future.delayed(Duration(seconds: 3), () => {
          LoginBloc.loadingI(context,false),



          for (var index = 0; index < GlobalVariables.listDetalleAgrupado.length; index++){
            //print('Editar...---- ' + GlobalVariables.listDetalleAgrupado[index].campo),
            if(GlobalVariables.listDetalleAgrupado[index].campo.contains('CP')){
              print('Editar...---- ' + GlobalVariables.listDetalleAgrupado[index].valor),
              GlobalVariables.controllerCPadd.text = GlobalVariables.listDetalleAgrupado[index].valor,
              ImcRBloc.getAdressCP(context,GlobalVariables.listDetalleAgrupado[index].valor),
              ImcRBloc.getCatalogosAdress(context,ApiDefinition.wsAdressCatEstadoCP,'',4),

            } else if(GlobalVariables.listDetalleAgrupado[index].campo.contains('COLONIA') ){
              print('COLONIA...---- ' + GlobalVariables.listDetalleAgrupado[index].valor),
              ImcRBloc.getCatalogosAdress(context, ApiDefinition.wsAdressCatLocalidadCP,GlobalVariables.listDetalleAgrupado[index].valor, 7)
            }
              else if(GlobalVariables.listDetalleAgrupado[index].campo.contains('LOCALIDAD') ){
                print('LOCALIDAD...---- ' + GlobalVariables.listDetalleAgrupado[index].valor),
                ImcRBloc.getCatalogosAdress(context,ApiDefinition.wsAdressCatColoniaCP, GlobalVariables.listDetalleAgrupado[index].valor,6)
              }


          },

//        Navigator.pushReplacement(
//        context,
//        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioDetalleAgrupacionScreen(),
//        transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//        transitionDuration: Duration(seconds: 3),
//        )
//        )
          Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
              return InventarioDetalleAgrupacionScreen();
            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
              ).animate(animation), child: child,);
            },
                transitionDuration: Duration(seconds: 1)
            ), (Route route) => false,)

        });

          }, // handle your onTap here
          child: Container(
          padding: EdgeInsets.all(10),
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
            color:  (index % 2) == 0 ? Color.fromRGBO(2, 142, 201, 1): Color.fromRGBO(65, 190, 223, 1),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0.0),
                bottomRight: Radius.circular(0.0))),
        height: MediaQuery.of(context).size.height * 0.08,
        child: Row(children: <Widget>[

          Text(listaAgrupado[index].descripcion,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 13)),
          new Icon(Icons.supervisor_account,color: Colors.white),

        ]),
          ),
     // ),
  )
    ),

    ]),
  );
}


 void loadAllDatesIMC(BuildContext context,List<Proyects> listaProyects, int index) async {

   if(GlobalVariables.listIventario.isEmpty)
  print('Entrando a app cargando Ws -----> Inventario desde proyecto ///////' +  listaProyects[index].id.toString());

  getInvent(context, ApiDefinition.wsInventToProyect + listaProyects[index].id.toString());
  //print(GlobalVariables.listIventario[0].toString());

}
Future<bool> _getFutureBool() {
  return Future.delayed(Duration(milliseconds: 5000))
      .then((onValue) => true);
}

