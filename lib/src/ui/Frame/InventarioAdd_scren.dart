import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:isae/src/models/Proyects.dart';
import 'package:isae/src/ui/Frame/InventarioDetalleAddAgrupacion_scren.dart';

class InventarioAddScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => InventarioAddScreenState();

}

class InventarioAddScreenState extends State<InventarioAddScreen> {


  @override
  void initState(){
    super.initState();



  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();

    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,
      appBar:
//      PreferredSize(
//          preferredSize: Size.fromHeight(85.0),
        //     child:
        GlobalWidgets.topBar('AGREGAR INVENTARIO','Marco Antonio Moreno Silva', context, '$currentTime'.substring(0,10)),

          //),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),
      //drawer: GlobalWidgets.menuL(context),

      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        GlobalWidgets.menuHome(context,'$currentTime'.substring(0,10),botonRegresar(context)),

                        Container(
                          height: MediaQuery.of(context).size.height * 0.02,

                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.02,
                          child:  Text(GlobalVariables.DescripProyect, maxLines: 2, style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Color.fromRGBO(9, 99, 141, 1))),

                        ),
                    Container(

                          color: Color.fromRGBO(12, 160, 219, 1), // cOLORE DE FONDO CAMBIAR
                          child: Column(
                            children: <Widget>[
                              //--------var es------------
                              Container(
                                color: Color.fromRGBO(207, 227, 233, 1),
                                child: getProyectsAddInventario(context,GlobalVariables.listProyects),
                              ),
                            ],
                          ),
                        ),


                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );


  }



}
Widget botonRegresar(BuildContext context) {
  return Container(

    height: MediaQuery.of(context).size.height * 0.02,
  );
}



Widget getProyectsAddInventario(BuildContext context , List<Proyects> listaProyects) {
  DateTime now1 = DateTime.parse("2020-05-20 20:18:00");
  var currentTime1 =
  new DateTime(now1.year, now1.month, now1.day, now1.hour, now1.minute);

  DateTime now2 = DateTime.parse("2020-05-21 16:14:00");
  var currentTime2 =
  new DateTime(now2.year, now2.month, now2.day, now2.hour, now2.minute);

  DateTime now3 = DateTime.parse("2020-05-15 19:42:00");
  var currentTime3 =
  new DateTime(now3.year, now3.month, now3.day, now3.hour, now3.minute);

  var color1 = Colors.amberAccent;
  var color2 = Colors.deepOrange;


  return Container(
    color: Color.fromRGBO(207, 227, 233, 1),

    padding: EdgeInsets.all(20),

    child: Column(children: <Widget>[

      for(var index = 0; index < listaProyects.length; index++)(


          InkWell(
            onTap: () {
              print('PASAR A AGRUPADOR....' );

              limpiar();
              LoginBloc.loadingI(context,true);
              GlobalVariables.idProyect = GlobalVariables.listProyects[index].id;
              GlobalVariables.nameProyectFile = GlobalVariables.listProyects[index].descripcion;
              GlobalVariables.idIventario =  1;
              ImcRBloc.loadInventarioAgrupado(context, GlobalVariables.idProyect, 0);
              Future.delayed(Duration(seconds: 3), () => {
              GlobalVariables.idAgrupador = GlobalVariables.listAgrupado[0].id,
              GlobalVariables.nameAgrupador = GlobalVariables.listAgrupado[0].descripcion,

              print('Detalle Campos ..........    ' + GlobalVariables.listAgrupado[0].id.toString() + ' Agrpador Add :: ' +  GlobalVariables.nameAgrupador),
              GlobalVariables.valor = 0,

              ImcRBloc.loadInventarioDetalleAgrupado(context,2),


              GlobalVariables.banderafirmaAdd ='1',

              Future.delayed(Duration(seconds: 2), () => {
                LoginBloc.loadingI(context,false),

                Navigator.pushAndRemoveUntil(
                  context,
                  PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                    return InventarioDetalleAddAgrupacionScreen();
                  }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                    return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                    ).animate(animation), child: child,);
                  },
                      transitionDuration: Duration(seconds: 1)
                  ), (Route route) => false,)

              })
            });



            }, // handle your onTap here
            child: Container(
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color:  (index % 2) == 0 ? Color.fromRGBO(9, 99, 141, 1): Color.fromRGBO(65, 190, 223, 1),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0))),
              height: MediaQuery.of(context).size.height * 0.08,
              child: Row(children: <Widget>[
                new Icon(Icons.label_important,color: Colors.white),
                Text(listaProyects[index].descripcion,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 14)),
                Expanded(
                  child: Container(
                    //padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
                    alignment: Alignment.bottomRight,
                    child: Text( listaProyects[index].fecha,  style: TextStyle(fontSize: 11, color: Colors.white)),


                  ),
                ),
                Expanded(
                  child: Container(
                    //padding: EdgeInsets.fromLTRB(100, 1, 11, 10),
                    alignment: Alignment.bottomRight,
                    child: new Icon(Icons.add,color: Colors.white),
                  ),
                ),
              ]),
            ),
            // ),
          )
      ),

    ]),
  );
}

void limpiar () async{
  GlobalVariables.selectedItemCatPROYECTO= null;
  GlobalVariables.selectedItemCatPROYECTODESCRIPCION= null;
  GlobalVariables.selectedItemCatFCREACON= null;
  GlobalVariables.selectedItemCatFOLIO= null;
  GlobalVariables.selectedItemCatID= null;
  GlobalVariables.selectedItemCatAPELLIDOS= null;
  GlobalVariables.selectedItemCatAPELLIDOS2= null;
  GlobalVariables.selectedItemCatNOMBRES= null;
  GlobalVariables.selectedItemCatNOMBRECOMPLETO= null;
  GlobalVariables.selectedItemCatNUMEMPLEADO= null;
  GlobalVariables.selectedItemCatVIP= null;
  GlobalVariables.selectedItemCatPUESTO= null;
  GlobalVariables.selectedItemCatDIRECCION= null;
  GlobalVariables.selectedItemCatSUBDIRECCION= null;
  GlobalVariables.selectedItemCatCLAVESUBDIRECCION= null;
  GlobalVariables.selectedItemCatGERENCIA= null;
  GlobalVariables.selectedItemCatCLAVEGERENCIA= null;
  GlobalVariables.selectedItemCatDEPTO= null;
  GlobalVariables.selectedItemCatCLAVECENTROTRABAJO= null;
  GlobalVariables.selectedItemCatCORREO= null;
  GlobalVariables.selectedItemCatTELEFONO= null;
  GlobalVariables.selectedItemCatEXT= null;
  GlobalVariables.selectedItemCatUBICACION= null;
  GlobalVariables.selectedItemCatESTADO= null;
  GlobalVariables.selectedItemCatCP= null;
  GlobalVariables.selectedItemCatCOLONIA= null;
  GlobalVariables.selectedItemCatUBICACIONCOMPLETA= null;
  GlobalVariables.selectedItemCatZONA= null;
  GlobalVariables.selectedItemCatLOCALIDAD= null;
  GlobalVariables.selectedItemCatEDIFICIO= null;
  GlobalVariables.selectedItemCatPISO= null;
  GlobalVariables.selectedItemCatAREA= null;
  GlobalVariables.selectedItemCatADSCRIPCION= null;
  GlobalVariables.selectedItemCatAPELLIDOSJEFE= null;
  GlobalVariables.selectedItemCatNOMBRESJEFE= null;
  GlobalVariables.selectedItemCatNOMBRECOMPLETOJEFE= null;
  GlobalVariables.selectedItemCatFICHAJEFE= null;
  GlobalVariables.selectedItemCatEXTJEFE= null;
  GlobalVariables.selectedItemCatUBICACIONJEFE= null;
  GlobalVariables.selectedItemCatNOMBREJEFEINMEDIATO= null;
  GlobalVariables.selectedItemCatAPELLIDOSRESGUARDO= null;
  GlobalVariables.selectedItemCatNOMBRESRESGUARDO= null;
  GlobalVariables.selectedItemCatNOMBRECOMPLETORESGUARDO= null;
  GlobalVariables.selectedItemCatADSCRIPCIONRESGUARDO= null;
  GlobalVariables.selectedItemCatEXTRESGUARDO= null;
  GlobalVariables.selectedItemCatAPELLIDOSRESPONSABLE= null;
  GlobalVariables.selectedItemCatNOMBRESRESPONSABLE= null;
  GlobalVariables.selectedItemCatNOMBRECOMPLETORESPONSABLE= null;
  GlobalVariables.selectedItemCatAPELLIDOSPEMEX= null;
  GlobalVariables.selectedItemCatNOMBRESPEMEX= null;
  GlobalVariables.selectedItemCatNOMBRECOMPLETOPEMEX= null;
  GlobalVariables.selectedItemCatTIPOEQUIPO= null;
  GlobalVariables.selectedItemCatEQUIPO= null;
  GlobalVariables.selectedItemCatMARCAEQUIPO= null;
  GlobalVariables.selectedItemCatMODELOEQUIPO= null;
  GlobalVariables.selectedItemCatNUMSERIEEQUIPO= null;
  GlobalVariables.selectedItemCatEQUIPOCOMPLETO= null;
  GlobalVariables.selectedItemCatMONITOR= null;
  GlobalVariables.selectedItemCatMARCAMONITOR= null;
  GlobalVariables.selectedItemCatMODELOMONITOR= null;
  GlobalVariables.selectedItemCatNUMSERIEMONITOR= null;
  GlobalVariables.selectedItemCatMONITORCOMPLETO= null;
  GlobalVariables.selectedItemCatTECLADO= null;
  GlobalVariables.selectedItemCatMARCATECLADO= null;
  GlobalVariables.selectedItemCatMODELOTECLADO= null;
  GlobalVariables.selectedItemCatNUMSERIETECLADO= null;
  GlobalVariables.selectedItemCatTECLADOCOMPLETO= null;
  GlobalVariables.selectedItemCatMOUSE= null;
  GlobalVariables.selectedItemCatMARCAMOUSE= null;
  GlobalVariables.selectedItemCatMODELOMAUSE= null;
  GlobalVariables.selectedItemCatNUMSERIEMOUSE= null;
  GlobalVariables.selectedItemCatMOUSECOMPLETO= null;
  GlobalVariables.selectedItemCatUPS= null;
  GlobalVariables.selectedItemCatMARCAUPS= null;
  GlobalVariables.selectedItemCatMODELOUPS= null;
  GlobalVariables.selectedItemCatNUMSERIEUPS= null;
  GlobalVariables.selectedItemCatUPSCOMPLETO= null;
  GlobalVariables.selectedItemCatMALETIN= null;
  GlobalVariables.selectedItemCatMARCAMALETIN= null;
  GlobalVariables.selectedItemCatMODELOMALETIN= null;
  GlobalVariables.selectedItemCatNUMSERIEMALETIN= null;
  GlobalVariables.selectedItemCatMALETINCOMLETO= null;
  GlobalVariables.selectedItemCatCANDADO= null;
  GlobalVariables.selectedItemCatMARCACANDADO= null;
  GlobalVariables.selectedItemCatMODELOCANDADO= null;
  GlobalVariables.selectedItemCatNUMSERIECANDADO= null;
  GlobalVariables.selectedItemCatCANDADOCOMPLETO= null;
  GlobalVariables.selectedItemCatBOCINAS= null;
  GlobalVariables.selectedItemCatMARCABOCINAS= null;
  GlobalVariables.selectedItemCatMODELOBOCINAS= null;
  GlobalVariables.selectedItemCatNUMSERIEBOCINAS= null;
  GlobalVariables.selectedItemCatBOCINASCOMPLETO= null;
  GlobalVariables.selectedItemCatCAMARA= null;
  GlobalVariables.selectedItemCatMARCACAMARA= null;
  GlobalVariables.selectedItemCatMODELOCAMARA= null;
  GlobalVariables.selectedItemCatNUMSERIECMARA= null;
  GlobalVariables.selectedItemCatCAMARACOMPLETO= null;
  GlobalVariables.selectedItemCatMONITOR2= null;
  GlobalVariables.selectedItemCatMARCAMONITOR2= null;
  GlobalVariables.selectedItemCatMODELOMONITOR2= null;
  GlobalVariables.selectedItemCatNUMSERIEMONITOR2= null;
  GlobalVariables.selectedItemCatMONITOR2COMPLETO= null;
  GlobalVariables.selectedItemCatACCESORIO= null;
  GlobalVariables.selectedItemCatMARCAACCESORIO= null;
  GlobalVariables.selectedItemCatMODELOACCESORIO= null;
  GlobalVariables.selectedItemCatNUMSERIEACCESORIO= null;
  GlobalVariables.selectedItemCatACCESORIOCOMPLETO= null;
  GlobalVariables.selectedItemCatRAM= null;
  GlobalVariables.selectedItemCatDISCODURO= null;
  GlobalVariables.selectedItemCatPROCESADOR= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP1= null;
  GlobalVariables.selectedItemCatMODELOCOMP1= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP1= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP1= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP2= null;
  GlobalVariables.selectedItemCatMODELOCOMP2= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP2= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP2= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP3= null;
  GlobalVariables.selectedItemCatMODELOCOMP3= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP3= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP3= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP4= null;
  GlobalVariables.selectedItemCatMODELOCOMP4= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP4= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP4= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP5= null;
  GlobalVariables.selectedItemCatMODELOCOMP5= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP5= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP5= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP6= null;
  GlobalVariables.selectedItemCatMODELOCOMP6= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP6= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP6= null;
  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP7= null;
  GlobalVariables.selectedItemCatMODELOCOMP7= null;
  GlobalVariables.selectedItemCatNUMSERIECOMP7= null;
  GlobalVariables.selectedItemCatCRUCECLIENTECOMP7= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP1= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP2= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP3= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP4= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP5= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP6= null;
  GlobalVariables.selectedItemCatVALIDACIONCOMP7= null;
  GlobalVariables.selectedItemCatVALIDADOSCOMP= null;
  GlobalVariables.selectedItemCatTECNICONOMBRE= null;
  GlobalVariables.selectedItemCatDIA= null;
  GlobalVariables.selectedItemCatMES= null;
  GlobalVariables.selectedItemCatANIO= null;
  GlobalVariables.selectedItemCatREQESPECIAL1= null;
  GlobalVariables.selectedItemCatREQESPECIAL2= null;
  GlobalVariables.selectedItemCatOBSINV= null;
  GlobalVariables.selectedItemCatOBSRESGUARDO= null;
  GlobalVariables.selectedItemCatOBSEXTRAS1= null;
  GlobalVariables.selectedItemCatOBSEXTRAS2= null;
  GlobalVariables.selectedItemCatESTATUS= null;
  GlobalVariables.selectedItemCatFESCALACION= null;
  GlobalVariables.selectedItemCatCOMENTARIOSESCALACION= null;
  GlobalVariables.controllerPROYECTO.text = '';
  GlobalVariables.controllerPROYECTODESCRIPCION.text = '';
  GlobalVariables.controllerFCREACON.text = '';
  GlobalVariables.controllerFOLIO.text = '';
  GlobalVariables.controllerID.text = '';
  GlobalVariables.controllerAPELLIDOS.text = '';
  GlobalVariables.controllerAPELLIDOS2.text = '';
  GlobalVariables.controllerNOMBRES.text = '';
  GlobalVariables.controllerNOMBRECOMPLETO.text = '';
  GlobalVariables.controllerNUMEMPLEADO.text = '';
  GlobalVariables.controllerVIP.text = '';
  GlobalVariables.controllerPUESTO.text = '';
  GlobalVariables.controllerDIRECCION.text = '';
  GlobalVariables.controllerSUBDIRECCION.text = '';
  GlobalVariables.controllerCLAVESUBDIRECCION.text = '';
  GlobalVariables.controllerGERENCIA.text = '';
  GlobalVariables.controllerCLAVEGERENCIA.text = '';
  GlobalVariables.controllerDEPTO.text = '';
  GlobalVariables.controllerCLAVECENTROTRABAJO.text = '';
  GlobalVariables.controllerCORREO.text = '';
  GlobalVariables.controllerTELEFONO.text = '';
  GlobalVariables.controllerEXT.text = '';
  GlobalVariables.controllerUBICACION.text = '';
  GlobalVariables.controllerESTADO.text = '';
  GlobalVariables.controllerCP.text = '';
  GlobalVariables.controllerCOLONIA.text = '';
  GlobalVariables.controllerUBICACIONCOMPLETA.text = '';
  GlobalVariables.controllerZONA.text = '';
  GlobalVariables.controllerLOCALIDAD.text = '';
  GlobalVariables.controllerEDIFICIO.text = '';
  GlobalVariables.controllerPISO.text = '';
  GlobalVariables.controllerAREA.text = '';
  GlobalVariables.controllerADSCRIPCION.text = '';
  GlobalVariables.controllerAPELLIDOSJEFE.text = '';
  GlobalVariables.controllerAPELLIDOS2JEFE.text = '';
  GlobalVariables.controllerNOMBRESJEFE.text = '';
  GlobalVariables.controllerNOMBRECOMPLETOJEFE.text = '';
  GlobalVariables.controllerFICHAJEFE.text = '';
  GlobalVariables.controllerEXTJEFE.text = '';
  GlobalVariables.controllerUBICACIONJEFE.text = '';
  GlobalVariables.controllerNOMBREJEFEINMEDIATO.text = '';
  GlobalVariables.controllerAPELLIDOSRESGUARDO.text = '';
  GlobalVariables.controllerAPELLIDOS2RESGUARDO.text = '';
  GlobalVariables.controllerNOMBRESRESGUARDO.text = '';
  GlobalVariables.controllerNOMBRECOMPLETORESGUARDO.text = '';
  GlobalVariables.controllerADSCRIPCIONRESGUARDO.text = '';
  GlobalVariables.controllerEXTRESGUARDO.text = '';
  GlobalVariables.controllerAPELLIDOSRESPONSABLE.text = '';
  GlobalVariables.controllerAPELLIDOS2RESPONSABLE.text = '';
  GlobalVariables.controllerNOMBRESRESPONSABLE.text = '';
  GlobalVariables.controllerNOMBRECOMPLETORESPONSABLE.text = '';
  GlobalVariables.controllerAPELLIDOSPEMEX.text = '';
  GlobalVariables.controllerAPELLIDOS2PEMEX.text = '';
  GlobalVariables.controllerNOMBRESPEMEX.text = '';
  GlobalVariables.controllerNOMBRECOMPLETOPEMEX.text = '';
  GlobalVariables.controllerTIPOEQUIPO.text = '';
  GlobalVariables.controllerEQUIPO.text = '';
  GlobalVariables.controllerMARCAEQUIPO.text = '';
  GlobalVariables.controllerMODELOEQUIPO.text = '';
  GlobalVariables.controllerNUMSERIEEQUIPO.text = '';
  GlobalVariables.controllerEQUIPOCOMPLETO.text = '';
  GlobalVariables.controllerMONITOR.text = '';
  GlobalVariables.controllerMARCAMONITOR.text = '';
  GlobalVariables.controllerMODELOMONITOR.text = '';
  GlobalVariables.controllerNUMSERIEMONITOR.text = '';
  GlobalVariables.controllerMONITORCOMPLETO.text = '';
  GlobalVariables.controllerTECLADO.text = '';
  GlobalVariables.controllerMARCATECLADO.text = '';
  GlobalVariables.controllerMODELOTECLADO.text = '';
  GlobalVariables.controllerNUMSERIETECLADO.text = '';
  GlobalVariables.controllerTECLADOCOMPLETO.text = '';
  GlobalVariables.controllerMOUSE.text = '';
  GlobalVariables.controllerMARCAMOUSE.text = '';
  GlobalVariables.controllerMODELOMAUSE.text = '';
  GlobalVariables.controllerNUMSERIEMOUSE.text = '';
  GlobalVariables.controllerMOUSECOMPLETO.text = '';
  GlobalVariables.controllerUPS.text = '';
  GlobalVariables.controllerMARCAUPS.text = '';
  GlobalVariables.controllerMODELOUPS.text = '';
  GlobalVariables.controllerNUMSERIEUPS.text = '';
  GlobalVariables.controllerUPSCOMPLETO.text = '';
  GlobalVariables.controllerMALETIN.text = '';
  GlobalVariables.controllerMARCAMALETIN.text = '';
  GlobalVariables.controllerMODELOMALETIN.text = '';
  GlobalVariables.controllerNUMSERIEMALETIN.text = '';
  GlobalVariables.controllerMALETINCOMLETO.text = '';
  GlobalVariables.controllerCANDADO.text = '';
  GlobalVariables.controllerMARCACANDADO.text = '';
  GlobalVariables.controllerMODELOCANDADO.text = '';
  GlobalVariables.controllerNUMSERIECANDADO.text = '';
  GlobalVariables.controllerCANDADOCOMPLETO.text = '';
  GlobalVariables.controllerBOCINAS.text = '';
  GlobalVariables.controllerMARCABOCINAS.text = '';
  GlobalVariables.controllerMODELOBOCINAS.text = '';
  GlobalVariables.controllerNUMSERIEBOCINAS.text = '';
  GlobalVariables.controllerBOCINASCOMPLETO.text = '';
  GlobalVariables.controllerCAMARA.text = '';
  GlobalVariables.controllerMARCACAMARA.text = '';
  GlobalVariables.controllerMODELOCAMARA.text = '';
  GlobalVariables.controllerNUMSERIECMARA.text = '';
  GlobalVariables.controllerCAMARACOMPLETO.text = '';
  GlobalVariables.controllerMONITOR2.text = '';
  GlobalVariables.controllerMARCAMONITOR2.text = '';
  GlobalVariables.controllerMODELOMONITOR2.text = '';
  GlobalVariables.controllerNUMSERIEMONITOR2.text = '';
  GlobalVariables.controllerMONITOR2COMPLETO.text = '';
  GlobalVariables.controllerACCESORIO.text = '';
  GlobalVariables.controllerMARCAACCESORIO.text = '';
  GlobalVariables.controllerMODELOACCESORIO.text = '';
  GlobalVariables.controllerNUMSERIEACCESORIO.text = '';
  GlobalVariables.controllerACCESORIOCOMPLETO.text = '';
  GlobalVariables.controllerRAM.text = '';
  GlobalVariables.controllerDISCODURO.text = '';
  GlobalVariables.controllerPROCESADOR.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP1.text = '';
  GlobalVariables.controllerMODELOCOMP1.text = '';
  GlobalVariables.controllerNUMSERIECOMP1.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP1.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP2.text = '';
  GlobalVariables.controllerMODELOCOMP2.text = '';
  GlobalVariables.controllerNUMSERIECOMP2.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP2.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP3.text = '';
  GlobalVariables.controllerMODELOCOMP3.text = '';
  GlobalVariables.controllerNUMSERIECOMP3.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP3.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP4.text = '';
  GlobalVariables.controllerMODELOCOMP4.text = '';
  GlobalVariables.controllerNUMSERIECOMP4.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP4.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP5.text = '';
  GlobalVariables.controllerMODELOCOMP5.text = '';
  GlobalVariables.controllerNUMSERIECOMP5.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP5.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP6.text = '';
  GlobalVariables.controllerMODELOCOMP6.text = '';
  GlobalVariables.controllerNUMSERIECOMP6.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP6.text = '';
  GlobalVariables.controllerTIPOEQUIPOCOMP7.text = '';
  GlobalVariables.controllerMODELOCOMP7.text = '';
  GlobalVariables.controllerNUMSERIECOMP7.text = '';
  GlobalVariables.controllerCRUCECLIENTECOMP7.text = '';
  GlobalVariables.controllerVALIDACIONCOMP1.text = '';
  GlobalVariables.controllerVALIDACIONCOMP2.text = '';
  GlobalVariables.controllerVALIDACIONCOMP3.text = '';
  GlobalVariables.controllerVALIDACIONCOMP4.text = '';
  GlobalVariables.controllerVALIDACIONCOMP5.text = '';
  GlobalVariables.controllerVALIDACIONCOMP6.text = '';
  GlobalVariables.controllerVALIDACIONCOMP7.text = '';
  GlobalVariables.controllerVALIDADOSCOMP.text = '';
  GlobalVariables.controllerTECNICONOMBRE.text = '';
  GlobalVariables.controllerDIA.text = '';
  GlobalVariables.controllerMES.text = '';
  GlobalVariables.controllerANIO.text = '';
  GlobalVariables.controllerREQESPECIAL1.text = '';
  GlobalVariables.controllerREQESPECIAL2.text = '';
  GlobalVariables.controllerOBSINV.text = '';
  GlobalVariables.controllerOBSRESGUARDO.text = '';
  GlobalVariables.controllerOBSEXTRAS1.text = '';
  GlobalVariables.controllerOBSEXTRAS2.text = '';
  GlobalVariables.controllerESTATUS.text = '';
  GlobalVariables.controllerFESCALACION.text = '';
  GlobalVariables.controllerCOMENTARIOSESCALACION.text = '';
  GlobalVariables.controllerCAMPOLIBRE1.text = '';
  GlobalVariables.controllerCAMPOLIBRE2.text = '';
  GlobalVariables.controllerCAMPOLIBRE3.text = '';
  GlobalVariables.controllerCAMPOLIBRE4.text = '';
  GlobalVariables.controllerCAMPOLIBRE5.text = '';
  GlobalVariables.controllerCAMPOLIBRE6.text = '';
  GlobalVariables.controllerCAMPOLIBRE7.text = '';
  GlobalVariables.controllerCAMPOLIBRE8.text = '';
  GlobalVariables.controllerCAMPOLIBRE9.text = '';
  GlobalVariables.controllerCAMPOLIBRE10.text = '';
  GlobalVariables.controllerCAMPOLIBRE11.text = '';
  GlobalVariables.controllerCAMPOLIBRE12.text = '';
  GlobalVariables.controllerCAMPOLIBRE13.text = '';
  GlobalVariables.controllerCAMPOLIBRE14.text = '';
  GlobalVariables.controllerCAMPOLIBRE15.text = '';
  GlobalVariables.controllerCAMPOLIBRE16.text = '';
  GlobalVariables.controllerCAMPOLIBRE17.text = '';
  GlobalVariables.controllerCAMPOLIBRE18.text = '';
  GlobalVariables.controllerCAMPOLIBRE19.text = '';
  GlobalVariables.controllerCAMPOLIBRE20.text = '';
  GlobalVariables.controllerCAMPOID.text = '';

  GlobalVariables.selectedItemCatESTADO = null;

 GlobalVariables.listCatMarcaModeloEQUIPO.clear();
 GlobalVariables.listCatMarcaModeloMONITOR.clear();
 GlobalVariables.listCatMarcaModeloTECLADO.clear();
 GlobalVariables.listCatMarcaModeloMOUSE.clear();
 GlobalVariables.listCatMarcaModeloUPS.clear();
 GlobalVariables.listCatMarcaModeloMALETIN.clear();
 GlobalVariables.listCatMarcaModeloCANDADO.clear();
 GlobalVariables.listCatMarcaModeloBOCINAS.clear();
 GlobalVariables.listCatMarcaModeloCAMARA.clear();
 GlobalVariables.listCatMarcaModeloMONITORADICIONAL.clear();
 GlobalVariables.listCatMarcaModeloACCERIO.clear();

GlobalVariables.fileNoSerieFotoAddEQUIPO = null;
GlobalVariables.fileNoSerieFotoAddMONITOR= null;
GlobalVariables.fileNoSerieFotoAddTECLADO= null;
GlobalVariables.fileNoSerieFotoAddMOUSE= null;
GlobalVariables.fileNoSerieFotoAddUPS= null;
GlobalVariables.fileNoSerieFotoAddMALETIN= null;
GlobalVariables.fileNoSerieFotoAddCANDADO= null;
GlobalVariables.fileNoSerieFotoAddBOCINAS= null;
GlobalVariables.fileNoSerieFotoAddCAMARA= null;
GlobalVariables.fileNoSerieFotoAddMONITOR2= null;
GlobalVariables.fileNoSerieFotoAddACCESORIO= null;
GlobalVariables.fileNoSerieFotoAddCOMP1= null;
GlobalVariables.fileNoSerieFotoAddCOMP2= null;
GlobalVariables.fileNoSerieFotoAddCOMP3= null;
GlobalVariables.fileNoSerieFotoAddCOMP4= null;
GlobalVariables.fileNoSerieFotoAddCOMP5= null;
GlobalVariables.fileNoSerieFotoAddCOMP6= null;
GlobalVariables.fileNoSerieFotoAddCOMP7= null;

 GlobalVariables.fileNameFotoNumSerieAddEQUIPO = '';
 GlobalVariables.fileNameFotoNumSerieAddMONITOR= '';
 GlobalVariables.fileNameFotoNumSerieAddTECLADO= '';
 GlobalVariables.fileNameFotoNumSerieAddMOUSE= '';
 GlobalVariables.fileNameFotoNumSerieAddUPS= '';
 GlobalVariables.fileNameFotoNumSerieAddMALETIN= '';
 GlobalVariables.fileNameFotoNumSerieAddCANDADO= '';
 GlobalVariables.fileNameFotoNumSerieAddBOCINAS= '';
 GlobalVariables.fileNameFotoNumSerieAddCAMARA= '';
 GlobalVariables.fileNameFotoNumSerieAddMONITOR2= '';
 GlobalVariables.fileNameFotoNumSerieAddACCESORIO= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP1= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP2= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP3= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP4= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP5= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP6= '';
 GlobalVariables.fileNameFotoNumSerieAddCOMP7= '';


}


