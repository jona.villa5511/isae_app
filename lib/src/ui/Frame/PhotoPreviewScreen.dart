import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/ui/Frame/InventarioLoadFiles_scren.dart';
import 'package:photo_view/photo_view.dart';

class PhotoPreviewScreen extends StatefulWidget {



  @override
  _PhotoPreviewScreenState createState() => _PhotoPreviewScreenState();
}

class _PhotoPreviewScreenState extends State<PhotoPreviewScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:     Text(GlobalVariables.listaFilesInven[GlobalVariables.index].fileName,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 26)),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioLoadFilesScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      body: GestureDetector(
        child: Center(
            child: PhotoView(
              imageProvider: NetworkImage(GlobalVariables.listaFilesInven[GlobalVariables.index].fileUrl),
            )
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
