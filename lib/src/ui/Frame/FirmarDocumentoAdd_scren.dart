import 'dart:typed_data';


import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:isae/src/ui/Frame/Firma1Screen.dart';
import 'package:isae/src/ui/Frame/Inventario_scren.dart';
import 'package:flutter/services.dart';
class FirmarDocumentoAddScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => FirmarDocumentoAddScreenState();

}

class FirmarDocumentoAddScreenState extends State<FirmarDocumentoAddScreen> {
  //String remotePDFpath = "";
  StorageReference storageReference;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown]);
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    //print('Fecha del dia  $currentTime');
    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,
      appBar:
      //PreferredSize(
      //  preferredSize: Size.fromHeight(85.0),
      //   child:
      GlobalWidgets.topBar('Firmar Documento', 'Marco Antonio', context,
          '$currentTime'.substring(0, 10)),

      //    ),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),


      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.topCenter,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(9, 99, 141, 1),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(0.0),
                        bottomRight: Radius.circular(0.0))),

                child: Row(children: <Widget>[
                  new Icon(Icons.work, color: Colors.white),
                  Text('Proyecto: ' + GlobalVariables.nameProyectFile,
                      maxLines: 1, textAlign: TextAlign.center, style:
                      TextStyle(fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),

                ]
                ),
              ),
              GlobalWidgets.menuHome(context, '$currentTime'.substring(0, 10),
                  botonRegresar(context)),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.03,
                        ),
                        FlatButton(

                          onPressed: () =>
                          {

                            GlobalVariables.firma1 = ByteData(0),
                            GlobalVariables.firma2 = ByteData(0),
                            GlobalVariables.firma3 = ByteData(0),
                            GlobalVariables.firma4 = ByteData(0),

                            Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(
                                  pageBuilder: (BuildContext context,
                                      Animation animation,
                                      Animation secondaryAnimation) {
                                    return Firma1Screen();
                                  },
                                  transitionsBuilder: (BuildContext context,
                                      Animation<double> animation,
                                      Animation<double> secondaryAnimation,
                                      Widget child) {
                                    return SlideTransition(
                                      position: Tween<Offset>(
                                        begin: Offset(1.0, 0.0),
                                        end: Offset.zero,
                                      ).animate(animation), child: child,);
                                  },
                                  transitionDuration: Duration(seconds: 1)
                              ), (Route route) => false,)
                          },
                          color: Color.fromRGBO(74, 191, 37, 1),
                          padding: EdgeInsets.all(5.0),
                          textColor: Colors.white,
                          child: Column( // Replace with a Row for horizontal icon + text

                            children: <Widget>[
                              Icon(Icons.mode_edit),
                              Text("Firmar Documento",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),


                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.05,

                        ),


            ],
          ),
        ),
      ),
    );
  }
}

Widget botonRegresar(BuildContext context) {
  return Container(
child:   FlatButton(
  child:
  Column( // Replace with a Row for horizontal icon + text
    children: <Widget>[
      Image(image: new AssetImage('assets/images/previous.png'),
          width: 25, height: 25, color: Colors.white),
      Text("Regresar",
        textAlign: TextAlign.center,
        style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Colors.white),
      )
    ],
  ),
  onPressed: (){

    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(

          pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioScreen();
          },

          transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          },

          transitionDuration: Duration(seconds: 1)
      ),

          (Route route) => false,
    );
  },
),
  );
}


