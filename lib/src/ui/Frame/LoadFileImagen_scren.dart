import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/models/Files.dart';
import 'package:isae/src/ui/Frame/InventarioLoadFiles_scren.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:firebase_storage/firebase_storage.dart';
// Una pantalla que permite a los usuarios tomar una fotografía utilizando una cámara determinada.
class TakePictureFileScreen extends StatefulWidget {
  final CameraDescription camera;


  const TakePictureFileScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureFileScreenState createState() => TakePictureFileScreenState();
}

class TakePictureFileScreenState extends State<TakePictureFileScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;


  @override
  void initState() {
    super.initState();
    // Para visualizar la salida actual de la cámara, es necesario
    // crear un CameraController.
    _controller = CameraController(
      // Obtén una cámara específica de la lista de cámaras disponibles
      widget.camera,
      // Define la resolución a utilizar
      ResolutionPreset.medium,

    );
    getUserLocation();
    // A continuación, debes inicializar el controlador. Esto devuelve un Future!
    _initializeControllerFuture = _controller.initialize();
    disablePathProviderPlatformOverride = true;
  }

  @override
  void dispose() {
    // Asegúrate de deshacerte del controlador cuando se deshaga del Widget.
    _controller.dispose();
    super.dispose();
  }
  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();
    setState(() {
      GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);
    });

    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tomar Evidencia'),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioLoadFilesScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),

      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Si el Future está completo, muestra la vista previa
            return CameraPreview(_controller);
          } else {
            // De lo contrario, muestra un indicador de carga
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        // Agrega un callback onPressed
        onPressed: () async {
          // Toma la foto en un bloque de try / catch. Si algo sale mal,
          // atrapa el error.
          try {
            // Ensure the camera is initialized
            await _initializeControllerFuture;
            String fecha= new DateTime.now().toString().substring(25,26);

           print('fecha:: ' +DateTime.now().toString());
           // Construye la ruta donde la imagen debe ser guardada usando
            // el paquete path.
            final path = join(

              //
              (await getTemporaryDirectory()).path,
              '${GlobalVariables.idIventario}-Evidencia-${fecha}.png',
            );
            print('Rutaaaaa de la Foto:::: '+ path);
            // Attempt to take a picture and log where it's been saved
            await _controller.takePicture(path);

            // En este ejemplo, guarda la imagen en el directorio temporal. Encuentra
            // el directorio temporal usando el plugin `path_provider`.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // Si se produce un error, regístralo en la consola.
            print(e);
          }
        },
      ),

    );
  }




}

// Un Widget que muestra la imagen tomada por el usuario
class DisplayPictureScreen extends StatelessWidget {


  StorageReference storageReference;
  final String imagePath;
  String _uploadedFileURL;
  DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);
  @override
  void initState(){
    getUserLocation();
  }

  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();

    GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);


    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Evidencia'),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioLoadFilesScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      // La imagen se almacena como un archivo en el dispositivo. Usa el
      // constructor `Image.file` con la ruta dada para mostrar la imagen
      body:
      Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[

              Container(
                height: MediaQuery.of(context).size.height * 0.03,

              ),
              Container(

                  child:   Image.file(File(imagePath),width: 500, height: 500),

              ),


              Container(
                height: MediaQuery.of(context).size.height * 0.03,

              ),

              Material(
                elevation: 5.0,
                //borderRadius: BorderRadius.circular(30.0),
                color:  Color.fromRGBO(74, 191, 37, 1),
                child: MaterialButton(

                  //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () async {

                    print("Guardar::::...... ");

                    String fileName = File(imagePath).path.split('/').last;

                    print(fileName);

                    // String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();
                    print('cargo foto ' + fileName);

                    LoginBloc.loadingI(context,true);


                    Directory documentDirectory = await getApplicationDocumentsDirectory();
                    String documentPath = documentDirectory.path;
                    String fileCopy = "$documentPath/$fileName";
                    File temp = File(imagePath);
                    File newFile = await temp.copy(fileCopy);
                    print('path antes:: ' + imagePath);
                     File fileFinal = new File.fromUri(Uri.parse(newFile.path));
                    print('fileFinal:: ' + fileFinal.path);
                    //_showDialog(context, 'fileFinal ' + fileFinal.path);
//                    uploadFileASW(context,File(fileFinal.path), fileName,files);
//                    LoginBloc.loadingI(context,true);


                    StorageReference storageReference = FirebaseStorage.instance
                        .ref()
                        .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idIventario}-Inventario/Evidencias/$fileName');
                    StorageUploadTask uploadTask = storageReference.putFile(temp);
                    await uploadTask.onComplete;
                    print('File Uploaded');

                    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
                    _uploadedFileURL = dowurl.toString();

                    print('urllllllllll  ' +_uploadedFileURL);

                    Files files = new Files(
                        0,
                        fileName,
                        _uploadedFileURL,
                        GlobalVariables.center.toString(),
                        GlobalVariables.user.id,
                        null,
                        GlobalVariables.idIventario,
                        GlobalVariables.idProyect,
                        'jpg'
                    );
                    print('Files ' + files.toString());

                    ImcRBloc.InsertFiles(context, files);
                    Future.delayed(Duration(seconds: 2), () => {
                      ImcRBloc.getFilesIvent(context)
                    });
                    Future.delayed(Duration(seconds: 4), () => {
                      LoginBloc.loadingI(context, false),
                      Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioLoadFilesScreen(),
                            transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
                            transitionDuration: Duration(seconds: 3),
                          )
                      )
                    });


                  },
                  textColor: Colors.white,
                  child: Text("Guardar",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
  void getFiles(BuildContext context) async{
    ImcRBloc.getFilesIvent(context);
  }
  void _showDialog( BuildContext context, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert"),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


}
Widget getGoogleMaps(BuildContext context, LatLng _center, Map<String, Marker> _markers) {
  if(_center != null)
    return Container(
      height: MediaQuery.of(context).size.height * 2.0,
      child:GoogleMap(
        myLocationButtonEnabled: true,
        buildingsEnabled: true,
        myLocationEnabled: true,
        trafficEnabled: true,
        mapType: MapType.hybrid,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 17,
        ),
        markers: _markers.values.toSet(),
      ),
    );
}