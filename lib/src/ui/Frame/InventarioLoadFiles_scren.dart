import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/complements/globalVariables.dart';
import 'package:image_picker/image_picker.dart';
import 'package:isae/src/models/Files.dart';
import 'package:isae/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:isae/src/ui/Frame/InventarioDetalle_scren.dart';
import 'package:isae/src/ui/Frame/PdfPreviewScreen.dart';
import 'package:isae/src/ui/Frame/ViewImageFileScreen.dart';
import 'package:open_file/open_file.dart';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';

import 'package:http/http.dart' as http;
class InventarioLoadFilesScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => InventarioLoadFilesScreenState();

}

class InventarioLoadFilesScreenState extends State<InventarioLoadFilesScreen> {
  File _pickedImage;
  String URLS;
  String _pathUploadFile = '';
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = new TextEditingController();
  String _fileName;
  String _pathFile;
  List<StorageUploadTask> _tasks = <StorageUploadTask>[];
  StorageReference storageReference;
  final Dio _dio = Dio();
  String _progress = "-";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  final FirebaseStorage _storage =
  FirebaseStorage(storageBucket: 'gs://isae-de6da.appspot.com');

  StorageUploadTask _uploadTask;

  @override
  void dispose(){
    super.dispose();
  }
  Future<void> turnOffNotification(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> turnOffNotificationById(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
      num id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> initNotification() async {

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final android = AndroidInitializationSettings('@mipmap/app_icon');
  final iOS = IOSInitializationSettings();
  final initSettings = InitializationSettings(android, iOS);
  flutterLocalNotificationsPlugin.initialize(initSettings, onSelectNotification: _onSelectNotification);
}
  @override
  void initState(){
    getFiles(context);
    super.initState();
    disablePathProviderPlatformOverride = true;

    initNotification();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());

  }
  Future<void> _onSelectNotification(String json) async {
    final obj = jsonDecode(json);

    if (obj['isSuccess'] && GlobalVariables.banderaDownload) {
      OpenFile.open(obj['filePath']);
      GlobalVariables.banderaDownload = false;
    } else {

    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails(
        'channel id',
        'channel name',
        'channel description',
        priority: Priority.High,
        importance: Importance.Max
    );
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android, iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];

    await flutterLocalNotificationsPlugin.show(
        0, // notification id
        isSuccess ? 'Descargado' : 'Fallo',
        isSuccess ? 'Archivo se ha descargado correctamente!' : 'Ocurrio un error al Descargar.',
        platform,
        payload: json
    );
  }

  Future<Directory> _getDownloadDirectory() async {

    return await getApplicationDocumentsDirectory();
  }

  Future<bool> _requestPermissions() async {

    return true;
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        _progress = (received / total * 100).toStringAsFixed(0) + "%";
      });
    }
  }

  Future<void> _startDownload(String savePath,String _fileURL) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      final response = await _dio.download(
          _fileURL,
          savePath,
          onReceiveProgress: _onReceiveProgress
      );
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
      LoginBloc.Downloading(context,false);
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  Future<void> _download(String _fileURL, String _nombreArchivo) async {
    final dir = await _getDownloadDirectory();

    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      final savePath = path.join(dir.path, _nombreArchivo);
      await _startDownload(savePath,_fileURL);
    } else {
      // handle the scenario when user declines the permissions
    }
  }

  getPDF() async{
    print('entro getPFD ' +GlobalVariables.urlPathPDF);
    GlobalVariables.doc = await PDFDocument.fromURL(GlobalVariables.urlPathPDF);

  }

  showAlertDeleteFile(BuildContext context) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirmar"),
      content: Text("¿Esta seguro de elimnar el archivo?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Future<void> _refresh() async {
    return  getFiles(context);

  }
  @override
  Widget build(BuildContext context) {
    getFiles(context);
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    //print('Fecha del dia  $currentTime');
    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,
      appBar:
//      PreferredSize(
//        preferredSize: Size.fromHeight(85.0),
//        child:
        GlobalWidgets.topBar('CARGA DE EVIDENCIAS','Marco Antonio', context, '$currentTime'.substring(0,10)),

     //  ),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),


      body:  Material(
        child:       RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
    child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.topCenter,
                          decoration: BoxDecoration(
                              color:  Color.fromRGBO(9, 99, 141, 1),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(0.0),
                                  bottomRight: Radius.circular(0.0))),

                          child:  Row ( children: <Widget>[
                            new Icon(Icons.work,color: Colors.white),
                            Text('Proyecto: ' + GlobalVariables.nameProyectFile, maxLines: 1, textAlign: TextAlign.center, style:
                            TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.02,
                            ),
                            new Icon(Icons.library_books,color: Colors.white),
                            Text('Inventario: ' + GlobalVariables.nameInvenFolioFile, maxLines: 1, textAlign: TextAlign.center, style:
                            TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),

                          ]
                          ),
                        ),
                        GlobalWidgets.menuHome(context,'$currentTime'.substring(0,10),botonRegresar(context)),


   Container(

                          color: Color.fromRGBO(207, 227, 233, 1),// cOLORE DE FONDO CAMBIAR
                          child: Column(
                            children: <Widget>[

                              //for (var index = 0; index < GlobalVariables.listaFilesInven.length; index++)(

                                  getLoadFilesDataBase(context)

                              //),


                            ],

                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        ),
      ),
    );


  }

  Widget botonRegresar(BuildContext context) {
    return Container(
    child:
    FlatButton(
      child:
      Column( // Replace with a Row for horizontal icon + text
        children: <Widget>[
          Image(image: new AssetImage('assets/images/previous.png'),
              width: 25, height: 25, color: Colors.white),
          Text("Regresar",
            textAlign: TextAlign.center,
            style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Colors.white),
          )
        ],
      ),
      onPressed: (){
        GlobalVariables.listFiles.clear();

        Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioDetalleScreen();
          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
            ).animate(animation), child: child,);
          },
              transitionDuration: Duration(seconds: 1)
          ), (Route route) => false,);
      },
    ),
    );
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }
  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/teste.pdf');
  }
  Future<File> writeCounter(Uint8List stream) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsBytes(stream);
  }
  Future<Uint8List> fetchPost() async {
    final response = await http.get(GlobalVariables.urlPathPDF);
    final responseJson = response.bodyBytes;

    return responseJson;
  }

  loadPdf() async {
    writeCounter(await fetchPost());
    URLS = (await _localFile).path;

    if (!mounted) return;

    setState(() {});
  }

  String _uploadedFileURL;

  void _openFileExplorer() async {
    setState(() => _loadingPath = true);
    try {
      if (_multiPick) {
        _pathFile = null;
        _paths = await FilePicker.getMultiFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
      } else {
        _paths = null;
        _pathFile = await FilePicker.getFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
        print('path ' + _pathFile);
        File file = new File(_pathFile);
        String fileName = file.path.split('/').last;

        print(fileName);
        if(_pathFile.contains('pdf') || _pathFile.contains('xls')){
          print('es correecto el formato ');
          int numeroconsecu = GlobalVariables.listaFilesInven.length+1;
          //String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();

          LoginBloc.loadingI(context,true);

          Directory documentDirectory =
          await getApplicationDocumentsDirectory();
          String documentPath = documentDirectory.path;
          _pathUploadFile = "$documentPath/$fileName";
          File temp = File(_pathFile);
          File newFile = await temp.copy(_pathUploadFile);
          print('path:: ' + newFile.path);
         // File fileFinal = new File.fromUri(Uri.parse(newFile.path));
          print('fileFinal:: ' + newFile.path);


          StorageReference storageReference = FirebaseStorage.instance
              .ref()
              .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idIventario}-Inventario/Evidencias/$fileName');
          StorageUploadTask uploadTask = storageReference.putFile(temp);
          await uploadTask.onComplete;
          print('File Uploaded');

          var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
          _uploadedFileURL = dowurl.toString();

          print('urllllllllll  ' +_uploadedFileURL);

          Files files = new Files(
              0,
              fileName,
              _uploadedFileURL,
              GlobalVariables.center.toString(),
              GlobalVariables.user.id,
              null,
              GlobalVariables.idIventario,
              GlobalVariables.idProyect,
              'pdf'
          );

          ImcRBloc.InsertFiles(context, files);
          Future.delayed(Duration(seconds: 2), () => {
            ImcRBloc.getFilesIvent(context)
          });
          Future.delayed(Duration(seconds: 4), () => {
            LoginBloc.loadingI(context, false),

            Navigator.pushAndRemoveUntil(
              context,
              PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                return InventarioLoadFilesScreen();
              }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                ).animate(animation), child: child,);
              },
                  transitionDuration: Duration(seconds: 1)
              ), (Route route) => false,)
          });


        }else{
          print('formato incorrecto');
          showDialog(context: context, builder: (context) => ShowMessage('Formato incorrecto solo admite PDF ò XLSX.'));
        }



      }
    } catch (e) {
      print("Error al cargar::: " + e.toString());
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName = _pathFile != null
          ? _pathFile.split('/').last
          : _paths != null ? _paths.keys.toString() : '...';
    });
  }

  static Future<String> getFileNameWithExtension(File file)async{

    if(await file.exists()){
      //To get file name without extension
      //path.basenameWithoutExtension(file.path);

      //return file with file extension
      return path.basename(file.path);
    }else{
      return null;
    }
  }

  Future<void> handleClick(String value, int index) async {
    switch (value) {
      case 'Descargar':
        print('descargar...' +index.toString());
        LoginBloc.Downloading(context,true);

        GlobalVariables.banderaDownload = true;
        _download(GlobalVariables.listaFilesInven[index].fileUrl, GlobalVariables.listaFilesInven[index].fileName);

        break;
      case 'Eliminar':
        showAlertDialogDeleteFiles(context, index);
        break;
      case 'Vista Previa':
        print('Vista previa....' + GlobalVariables.listaFilesInven[index].fileUrl == null ? '' : GlobalVariables.listaFilesInven[index].fileUrl);
                      GlobalVariables.urlPathPDF = GlobalVariables.listaFilesInven[index].fileUrl == null ? '' : GlobalVariables.listaFilesInven[index].fileUrl;
                      GlobalVariables.nombrePDF = GlobalVariables.listaFilesInven[index].fileName == null ? '' : GlobalVariables.listaFilesInven[index].fileName;
                      GlobalVariables.index = index;
                      //loadPdf();

                      if(GlobalVariables.listaFilesInven[index].fileName.contains('pdf')) {
                        getPDF();
                        LoginBloc.loadingI(context,true);
                        Future.delayed(Duration(seconds: 3), () => {
                          LoginBloc.loadingI(context, false),


                          Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                              return PdfPreviewScreen();
                            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                              ).animate(animation), child: child,);
                            },
                                transitionDuration: Duration(seconds: 1)
                            ), (Route route) => false,)
                        });

                      } else if(GlobalVariables.listaFilesInven[index].fileName.contains('xls')) {
                        LoginBloc.loadingI(context,false);
                        showDialog(context: context, builder: (context) => ShowMessageVistaExcel('No es posible mostrar vista previa del archivo.','Descargar achivo para visualizarlo'));
                      } else{
                        LoginBloc.loadingI(context,true);
                        String coordenadas = GlobalVariables.listaFilesInven[index].fileCoordenadas.replaceAll('LatLng(', '').replaceAll(')', '');
                        var latLng  = coordenadas.split(",");
                        double latitude = double.parse(latLng[0]);
                        double longitude = double.parse(latLng[1]);
                        LatLng latlng = new LatLng(latitude, longitude);
                        print('COORDENADAS BASE::: ' + latlng.toString());

                        Set<Marker> markers = {};
                        Marker startMarker = Marker(
                          position: latlng,
                          markerId: MarkerId("mine"),
                          icon: BitmapDescriptor.defaultMarker,);
                        markers.add(startMarker);

                        final indexParam =index;
                        Future.delayed(Duration(seconds: 1), () => {
                          LoginBloc.loadingI(context,false),
                          //showDialog(context: context, builder: (context) => ShowPreviewImageDataBase(index,File(GlobalVariables.listaFilesInven[index].fileUrl),latlng,markers))

                        Navigator.pushReplacement(context,
                        PageRouteBuilder( pageBuilder: (context, anim1, anim2) => ViewImageFileScreenScreen(),
                        transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                        transitionDuration: Duration(seconds: 2),
                        )
                        )
                        });
                      }
        break;
    }
  }
  Widget getLoadFilesDataBase(BuildContext context ) {
    double iconSize = 15;
   // String fileName = GlobalVariables.listaFilesInven[index].fileName.split('/').last;
    return   Container(
        decoration: BoxDecoration(
            color: Color.fromRGBO(65, 190, 223, 1).withOpacity(0.2),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0))),
        child:

        Table(
    border: TableBorder.all(
              color: Color.fromRGBO(207, 227, 233, 1),
            ),
          columnWidths: {

            0: FlexColumnWidth(1.5),
            // 0: FlexColumnWidth(4.501), // - is ok
            // 0: FlexColumnWidth(4.499), //- ok as well
            1: FlexColumnWidth(1),
          },
          children: [
            TableRow( children: [
              Column(children:[
            Center(child:Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child:  Text('NOMBRE',textAlign: TextAlign.center, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black,)) )),
              ]),
              Column(children:[
            Center(child:Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Text('TIPO',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
              ]),
              Column(children:[
            Center(child:Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Text('FECHA',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
              ]),
              Column(children:[
                Center(child:Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child:Text('ACCIÒN',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
              ]),
            ]),

            for (var index = 0; index < GlobalVariables.listaFilesInven.length; index++)(

            TableRow( children: [
              //Text(GlobalVariables.listaFilesInven[index].fileName, maxLines: 2, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black)),
              Center(child:Container(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Text(GlobalVariables.listaFilesInven[index].fileName, maxLines: 4, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black,)),)),

              Center(child:Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: GlobalVariables.listaFilesInven[index].fileName.split('/').last.contains('pdf') ? Icon(Icons.picture_as_pdf,color: Color.fromRGBO(9, 99, 141, 1)) : GlobalVariables.listaFilesInven[index].fileName.split('/').last.contains('png') ?  Icon(Icons.image,color: Color.fromRGBO(9, 99, 141, 1))  :  Icon(Icons.explicit,color: Color.fromRGBO(9, 99, 141, 1)) )),

              Center(child:Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text(GlobalVariables.listaFilesInven[index].fecha.substring(0,10), maxLines: 2, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black)),)),


              PopupMenuButton<String>(
                    onSelected: (result){
                      handleClick(result, index);

                    },
                    itemBuilder: (BuildContext context) {
                      return {'Descargar', 'Eliminar','Vista Previa'}.map((String choice) {

                        return PopupMenuItem<String>(

                          value: choice,
                          child:  ListTile(
                            leading: choice == 'Descargar' ? Icon(Icons.file_download,color: Color.fromRGBO(12, 160, 219, 1)) : choice == 'Eliminar' ? Icon(Icons.delete_sweep,color: Color.fromRGBO(12, 160, 219, 1)) : Icon(Icons.visibility,color: Color.fromRGBO(12, 160, 219, 1)),
                            title:
                            Center(child:Container(
                              child: Text(choice,  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color.fromRGBO(12, 160, 219, 1))),)),
                          ),
                        );
                      }).toList();
                    },
                  ),
            ])
    ),
          ],
        ),

    );
  }
   showAlertDialogDeleteFiles(BuildContext context, int index) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {
        print('Eliminar....' + ' index ' + index.toString());
        Files files = new Files(
            GlobalVariables.listaFilesInven[index].id,
            null,
            null,
            GlobalVariables.center.toString(),
            GlobalVariables.user.id,
            null,
            GlobalVariables.idIventario,
            GlobalVariables.idProyect,
            ''
        );
        print('Files ' + files.id.toString());

        ImcRBloc.eliminaFiles(context, files);
        LoginBloc.loadingI(context,true);
        String filePath = 'Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idIventario}-Inventario/Evidencias/'+GlobalVariables.listaFilesInven[index].fileName;
        print('File deltete :.: ' + filePath);
        StorageReference storageReferance = FirebaseStorage.instance.ref();
        storageReferance.child(filePath).delete().then((_) => print('Successfully deleted $filePath storage item' ));
        Future.delayed(Duration(seconds: 2), () => {
          ImcRBloc.getFilesIvent(context)
        });
        Future.delayed(Duration(seconds: 4), () => {
          LoginBloc.loadingI(context, false),

          Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
              return InventarioLoadFilesScreen();
            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
              ).animate(animation), child: child,);
            },
                transitionDuration: Duration(seconds: 1)
            ), (Route route) => false,)
        });
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alerta"),
      content: Text("¿Esta seguro de eliminar evidencia?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Widget ShowMessageVistaExcel(String message, String text2) =>
      StreamBuilder<bool>(builder: (context, snap) {


        return AlertDialog(
          backgroundColor: Color.fromRGBO(207, 227, 233, 1),
          scrollable: true,
          title: new Text('Alerta'),

          content: Container(


            color: Color.fromRGBO(178, 222, 235, 1),
            child: Column(
              children: <Widget>[
                Text(message, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                Text(text2, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                Icon(Icons.report_problem, color: Colors.blue)
              ],
            ),
          ),
        );
      });

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed: () {

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Aviso!"),
      content: Text("¿Esta seguro de guardar los archivos permanentemente?."),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showMessageFiles(BuildContext context) {


    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {

        Navigator.of(context).pop();

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Aviso!"),
      content: Text("No existen archivos adjuntos!."),
      actions: [

        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  int numeroconsecu = GlobalVariables.listaFilesInven.length+1;
  void _pickImage(BuildContext context) async {

    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      _pickedImage = picture;
    });



    String fileName = _pickedImage.path.split('/').last;

    print(fileName);

    // String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();
    print('cargo foto ' + fileName);


    Files files = new Files(
        0,
        fileName,
        'https://app-despliegue.s3.us-east-2.amazonaws.com/'+fileName,
        GlobalVariables.center.toString(),
        GlobalVariables.user.id,
        null,
        GlobalVariables.idIventario,
        GlobalVariables.idProyect,
        'jpg'
    );
    print('Files ' + files.toString());



    //xuploadFileASW(context,_pickedImage, fileName,files);
    LoginBloc.loadingI(context,true);



  }



}



Widget ShowPreviewImageDataBase(int index, File _pickedImage, LatLng latlng, Set<Marker> markers) =>
    StreamBuilder<bool>(builder: (context, snap) {

      return AlertDialog(

          backgroundColor: Color.fromRGBO(207, 227, 233, 1),
          scrollable: true,
          title: Column(

            children: <Widget>[
              Text(GlobalVariables.listaFilesInven[index].fileName,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(9, 99, 141, 1),
                      fontSize: 26)),


            ],
          ),

          content:
          Column(
              children: <Widget>[
                FlatButton(
                  child:  Icon(Icons.keyboard_return, color: Colors.blue),
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => InventarioLoadFilesScreen()),
                    );
                  },
                ),
                InkWell(

                  onTap: () {
                    print('Compartir....'+ GlobalVariables.listaFilesInven[index].fileUrl);

//                    FlutterShareFile.shareImage(GlobalVariables.listaFilesInven[index].fileUrl, 'image.png');


                  },
                  child: Container(

                    padding: EdgeInsets.fromLTRB(200, 1, 1, 10),

                    child: Row ( children: <Widget>[

//                      Icon(Icons.share, color: Colors.blue),

                    ]
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.50,
                  child: _pickedImage == null
                      ? Text("Sin datos")

                      : Image.network(
                    GlobalVariables.listaFilesInven[index].fileUrl,
                    fit: BoxFit.cover,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes
                              : null,
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.03,

                ),


              ])


      );
    });



Widget getGoogleMaps(BuildContext context, LatLng _center, markers) {
  if(_center != null)
    return Container(
      height: MediaQuery.of(context).size.height * 2.0,
      child:GoogleMap(
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        buildingsEnabled: true,
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          bearing: 1.0,
          tilt: 2.0,
          target: _center,
          zoom: 17,
        ),
        markers: markers != null ? Set<Marker>.from(markers) : null,
      ),
    );
}