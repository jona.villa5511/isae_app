import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:isae/src/blocs/ISAE/isae_bloc.dart';
import 'package:isae/src/blocs/login_bloc.dart';
import 'package:isae/src/complements/globalWidgets.dart';
import 'package:isae/src/models/Files.dart';
import 'package:isae/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:isae/src/ui/Frame/Inventario_scren.dart';
import 'package:isae/src/ui/login_screen.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:isae/src/complements/globalVariables.dart';
// Una pantalla que permite a los usuarios tomar una fotografía utilizando una cámara determinada.
class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;


  @override
  void initState() {
    super.initState();
    // Para visualizar la salida actual de la cámara, es necesario
    // crear un CameraController.
    _controller = CameraController(
      // Obtén una cámara específica de la lista de cámaras disponibles
      widget.camera,
      // Define la resolución a utilizar
      ResolutionPreset.medium,

    );
    getUserLocation();
    //ShowMessajeInicio();
    // A continuación, debes inicializar el controlador. Esto devuelve un Future!
    _initializeControllerFuture = _controller.initialize();
  }


  @override
  void dispose() {
    // Asegúrate de deshacerte del controlador cuando se deshaga del Widget.
    _controller.dispose();
    super.dispose();
  }
  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  ShowMessajeInicio() async{

    showDialog(context: context, builder: (context) => ShowMessageValidateAdd('La Ubicaciòn no esta activada!','Favor de habilitar en Ajustes.'));

  }
  getUserLocation() async {
    print('entro locali');
    GlobalVariables.validateLocalizcion = true;
    try{

    GlobalVariables.currentLocation = await locateUser();
    setState(() {
      GlobalVariables.validateLocalizcion = true;
      GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);
    });
    print('validar ubicacion estad::: ' +   GlobalVariables.validateLocalizcion .toString());
  }on Exception catch (exception) {
      GlobalVariables.validateLocalizcion = false;
      showDialog(context: context, builder: (context) => ShowMessageValidateAdd('La Ubicaciòn no esta activada!','Favor de habilitar en Ajustes.'));
      print("throwing new error");
  throw Exception('errorr');
  }

    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tomar Foto'),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {


              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => LoginScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      // Debes esperar hasta que el controlador se inicialice antes de mostrar la vista previa
      // de la cámara. Utiliza un FutureBuilder para mostrar un spinner de carga
      // hasta que el controlador haya terminado de inicializar.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Si el Future está completo, muestra la vista previa
            return CameraPreview(_controller);
          } else {
            // De lo contrario, muestra un indicador de carga
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        // Agrega un callback onPressed
        onPressed: () async {
          // Toma la foto en un bloque de try / catch. Si algo sale mal,
          // atrapa el error.
          try {
            // Ensure the camera is initialized
            await _initializeControllerFuture;

            // Construye la ruta donde la imagen debe ser guardada usando
            // el paquete path.
            final path = join(

              //
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );

            // Attempt to take a picture and log where it's been saved
            await _controller.takePicture(path);

            // En este ejemplo, guarda la imagen en el directorio temporal. Encuentra
            // el directorio temporal usando el plugin `path_provider`.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // Si se produce un error, regístralo en la consola.
            print(e);
          }
        },
      ),

    );
  }




}

// Un Widget que muestra la imagen tomada por el usuario
class DisplayPictureScreen extends StatelessWidget {

  String _uploadedFileURL;
  StorageReference storageReference;
  final String imagePath;

  DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);
  @override
  void initState(){
    getUserLocation();
  }

  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();

    GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);


    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Foto de Incio de sesiòn'),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => LoginScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      // La imagen se almacena como un archivo en el dispositivo. Usa el
      // constructor `Image.file` con la ruta dada para mostrar la imagen
      body:
      Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[

              Image.file(File(imagePath),width: 250, height: 250),



              Container(
                height: MediaQuery.of(context).size.height * 0.02,
                child:  GlobalVariables.validateLocalizcion  == false ? Row ( children: <Widget>[
                new Icon(Icons.warning,color: Color.fromRGBO(12, 160, 219, 1)),
                  Text('La Ubicaciòn esta desabilitada.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Color.fromRGBO(9, 99, 141, 1))),

                ]):Text(''),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.15,
                  child:  getGoogleMaps(context, GlobalVariables.center, GlobalVariables.markers)

              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.02,

              ),
              Material(
                elevation: 5.0,
                //borderRadius: BorderRadius.circular(30.0),
                color:  Color.fromRGBO(74, 191, 37, 1),
                child: MaterialButton(

                  //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () async {
                    String fileName = '${GlobalVariables.user.id}-${GlobalVariables.user.nombreCompleto}.png';

                    print("Inicio::::...... ");
                    print(fileName);

                    // String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();
                    print('cargo foto ' + fileName);

                    LoginBloc.loadingI(context,true);


                    Directory documentDirectory = await getApplicationDocumentsDirectory();
                    String documentPath = documentDirectory.path;
                    String fileCopy = "$documentPath/$fileName";
                    File temp = File(imagePath);
                    File newFile = await temp.copy(fileCopy);
                    print('path antes:: ' + imagePath);
                    File fileFinal = new File.fromUri(Uri.parse(newFile.path));
                    print('fileFinal:: ' + fileFinal.path);



                    StorageReference storageReference = FirebaseStorage.instance
                        .ref()
                        .child('Sesion/Usuarios/$fileName');
                    StorageUploadTask uploadTask = storageReference.putFile(temp);
                    await uploadTask.onComplete;
                    print('File Uploaded');

                    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
                    _uploadedFileURL = dowurl.toString();

                    print('urllllllllll  ' +_uploadedFileURL);

                    Files files = new Files(
                        0,
                        fileName,
                        _uploadedFileURL,
                        GlobalVariables.center.toString(),
                        GlobalVariables.user.id,
                        null,
                        0,
                       0,
                        'jpg'
                    );
                    print('Files ' + files.toString());

                    ImcRBloc.InsertFilesOtros(context, files);

                    Future.delayed(Duration(seconds: 2), () => {

                      Navigator.pushReplacement(
                          context,
                          PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioScreen(),
                            transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
                            transitionDuration: Duration(seconds: 1),
                          )
                      )

                    });
                    //Navigator.pop(context);

                  },
                  textColor: Colors.white,
                  child: Text("Iniciar",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }





}
Widget getGoogleMaps(BuildContext context, LatLng _center, Map<String, Marker> _markers) {
  if(_center != null)
    return Container(
      height: MediaQuery.of(context).size.height * 2.0,
      child:GoogleMap(
        myLocationButtonEnabled: true,
        buildingsEnabled: true,
        myLocationEnabled: true,
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 17,
        ),
        markers: _markers.values.toSet(),
      ),
    );
}